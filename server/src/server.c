/******************************************************************************
 * Technische Informatik 2                                                    *
 *                                                                            *
 * Exercise 11 - Task 3                                                       *
 *                                                                            *
 * Student:                  Felix Rüdiger                                    *
 * Matrikel No.:             214002                                           *
 *                                                                            *
 * File:                     server.c                                         *
 *                                                                            *
 * Original source location: https://gitlab.com/fruediger-ti2/ue11            *
 *                                                                            *
 ******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <libgen.h>

#define BUFFER_SIZE (512)

static void usage(const char *executableName)
{
    printf("USAGE: %s PORT\n", executableName);
}

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        usage(basename(argv[0]));
        exit(EXIT_FAILURE);
    }

    int port;
    if (((port = atoi(argv[1])) < 1024) || (port > 65535))
    {
        printf("\"%s\" is not a port (number) in the range from 1024 to 65535.\n", argv[2]);
        printf("Try this instead:\n");
        usage(basename(argv[0]));
        exit(EXIT_FAILURE);
    }

    struct sockaddr_in server_addr = { .sin_family = AF_INET, .sin_port = htons(port), .sin_addr = { .s_addr = htonl(INADDR_ANY) } },
                       client_addr;
    socklen_t          client_addr_len;

    int sock;
    if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        printf("Could not create a new socket.\n");
        exit(EXIT_FAILURE);
    }

/*
    // to enable bind to the same (multicast enabled) address-port-combination on the same machine
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &(int){ 1 }, sizeof(int)) != 0)
    {
        //clean up
        close(sock);

        printf("Could not initialized newly created socket.\n");
        exit(EXIT_FAILURE);
    }

*/

    if (bind(sock, (struct sockaddr *)&server_addr, sizeof(struct sockaddr_in)) != 0)
    {
        printf("Could not bind socket to port %s.\n", argv[1]);
        exit(EXIT_FAILURE);
    }

    ssize_t count_received,
            count_written;
    char    buffer[BUFFER_SIZE];
    while ((count_received = recvfrom(sock, buffer, BUFFER_SIZE, 0, (struct sockaddr *)&client_addr, &client_addr_len)) >= 0)
    {
        count_written = 0;
        while ((count_written += write(STDOUT_FILENO, &buffer[count_written], count_received - count_written)) < count_received);
    }

    close(sock);

    return EXIT_SUCCESS;
}