/******************************************************************************
 * Technische Informatik 2                                                    *
 *                                                                            *
 * Exercise 11 - Task 3                                                       *
 *                                                                            *
 * Student:                  Felix Rüdiger                                    *
 * Matrikel No.:             214002                                           *
 *                                                                            *
 * File:                     client.c                                         *
 *                                                                            *
 * Original source location: https://gitlab.com/fruediger-ti2/ue11            *
 *                                                                            *
 ******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <libgen.h>

#define BUFFER_SIZE (512)

static void usage(const char *executableName)
{
    printf("USAGE: %s IP-ADDRESS PORT\n", executableName);
}

int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        usage(basename(argv[0]));
        exit(EXIT_FAILURE);
    }

    struct in_addr addr;
    if (inet_pton(AF_INET, argv[1], &addr) != 1)
    {
        printf("\"%s\" is not a valid ip address.\n", argv[1]);
        printf("Try this instead:\n");
        usage(basename(argv[0]));
        exit(EXIT_FAILURE);
    }

    int port;
    if (((port = atoi(argv[2])) < 1024) || (port > 65535))
    {
        printf("\"%s\" is not a port (number) in the range from 1024 to 65535.\n", argv[2]);
        printf("Try this instead:\n");
        usage(basename(argv[0]));
        exit(EXIT_FAILURE);
    }

    struct sockaddr_in server_addr = { .sin_family = AF_INET, .sin_port = htons(port), .sin_addr = addr                            },
                       client_addr = { .sin_family = AF_INET, .sin_port = htons(0),    .sin_addr = { .s_addr = htonl(INADDR_ANY) } };
    
    int sock;
    if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        printf("Could not create a new socket.\n");
        exit(EXIT_FAILURE);
    }

    if (bind(sock, (struct sockaddr *)&client_addr, sizeof(struct sockaddr_in)) != 0)
    {
        //clean up
        close(sock);

        printf("Could not bind socket to address %s:%s.\n", argv[1], argv[2]);
        exit(EXIT_FAILURE);
    }

    ssize_t count_read, count_send;
    char    buffer[BUFFER_SIZE];
    while ((count_read = read(STDIN_FILENO, buffer, BUFFER_SIZE)) > 0)
    {
        count_send = 0;
        ssize_t send;
        while ((count_send += (send = sendto(sock, &buffer[count_send], count_read - count_send, 0, (struct sockaddr *)&server_addr, sizeof(struct sockaddr_in)))) < count_read)
            if (send < 0)
            {
                perror("Could not completly transmit data.");
                break;
            }

    }

    close(sock);

    return EXIT_SUCCESS;
}